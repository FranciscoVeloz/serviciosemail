﻿namespace IP_7_ServicioEmails
{
    public interface IMensaje
    {
        void Mensaje(string body, string from, string to);
    }
}
