﻿using System;

namespace IP_7_ServicioEmails
{
    public class Gmail : IMensaje
    {
        public void Mensaje(string body, string from, string to)
        {
            Console.WriteLine($"Gmail message from: {from}, to: {to}... Body: {body}");
        }
    }
}
