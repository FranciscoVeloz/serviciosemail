﻿using System;

namespace IP_7_ServicioEmails
{
    public class Outlook : IMensaje
    {
        public void Mensaje(string body, string from, string to)
        {
            Console.WriteLine($"Outlook message from: {from}, to: {to}... Body: {body}");
        }
    }
}
