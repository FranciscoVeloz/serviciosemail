﻿using System;

namespace IP_7_ServicioEmails
{
    class Program
    {
        static void Main(string[] args)
        {
            var gmail = GenerarMensaje();
            gmail.Mensaje("This is a message", "Francisco", "Daneli");
            Console.ReadKey();
        }

        public static IMensaje GenerarMensaje()
        {
            IMensaje mensaje = new Gmail();
            return mensaje;
        }
    }
}
